from othelloGE import OthelloBoard
from othelloIA import Player
from time import sleep
import colorama

if __name__ == '__main__':
    colorama.init()
    board = OthelloBoard()
    p1, p2 = Player(1), Player(2)
    p1_tiles, p2_tiles = 2, 2
    turn = 1
    while p1_tiles + p2_tiles != 64:
        print('Turn', turn, ':')
        p1_move = p1.move(board.get_tiles(p1.id))
        if p1_move:
            print('Player', p1.id, 'plays on ({}, {})'.format(p1_move['x'], p1_move['y']))
            board.play(p1_move, p1.id)
            board.print_board()
        sleep(1)
        p2_move = p2.move(board.get_tiles(p2.id))
        if p2_move:
            print('Player', p2.id, 'plays on ({}, {})'.format(p2_move['x'], p2_move['y']))
            board.play(p2_move, p2.id)
            board.print_board()
        turn += 1
        p1_tiles, p2_tiles = board.get_tiles_number()
        print('Player', p1.id, ':', p1_tiles)
        print('Player', p2.id, ':', p2_tiles)
        if not p1_move and not p2_move:
            break
        sleep(1)
    if p1_tiles > p2_tiles:
        print('Player', p1.id, 'won')
    elif p2_tiles > p1_tiles:
        print('Player', p2.id, 'won')
    else:
        print('The game is a draw')

