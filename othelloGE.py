from colorama import Fore, Style

ROW_LEFT = 0
DIAG_DESC_LEFT = 1
COL_UP = 2
DIAG_ASC_RIGHT = 3
ROW_RIGHT = 4
DIAG_DESC_RIGHT = 5
COL_DOWN = 6
DIAG_ASC_LEFT = 7

class OthelloBoard():

    def __init__(self):
        self.board = [[0 for x in range(8)]for y in range(8)]
        self.board[3][3], self.board[4][4] = 1, 1
        self.board[3][4], self.board[4][3] = 2, 2

    def print_board(self):
        for row in self.board:
            print(" | ".join(['{}{}{}'.format(Fore.RED if elem == 1 else Fore.BLUE if elem == 2 else Fore.WHITE, elem, Style.RESET_ALL) for elem in row]))

    def get_tiles(self, player):
        get_tiles_func = [
            self.get_row_left,
            self.get_diag_desc_left,
            self.get_col_up,
            self.get_diag_asc_right,
            self.get_row_right,
            self.get_diag_desc_right,
            self.get_col_down,
            self.get_diag_asc_left
        ]
        moves = list()
        for x, row in enumerate(self.board):
            for y, tile in enumerate(row):
                if tile == player:
                    neighbours_opponent = self.next_to_opponent(x, y, 1 if player == 2 else 2)
                    for neighbour in neighbours_opponent:
                        moves.append(get_tiles_func[neighbour](x, y, player))
        return moves

    def next_to_opponent(self, x, y, opponent):
        neighbours = [
            dict(x=x, y=y - 1, direction=ROW_LEFT),
            dict(x=x - 1, y=y - 1, direction=DIAG_DESC_LEFT),
            dict(x=x - 1, y=y, direction=COL_UP),
            dict(x=x - 1, y=y + 1, direction=DIAG_ASC_RIGHT),
            dict(x=x, y=y + 1, direction=ROW_RIGHT),
            dict(x=x + 1, y=y + 1, direction=DIAG_DESC_RIGHT),
            dict(x=x + 1, y=y, direction=COL_DOWN),
            dict(x=x + 1, y=y - 1, direction=DIAG_ASC_LEFT)
        ]
        neighbours = filter(lambda elem: elem['x'] in range(8) and elem['y'] in range(8), neighbours)
        opponents = filter(lambda elem: self.board[elem['x']][elem['y']] == opponent, neighbours)
        return [elem['direction'] for elem in opponents]

    def get_row_left(self, x, y, player):
        y -= 1
        res = list()
        valid = False
        while y >= 0:
            res.append(dict(x=x, y=y))
            if self.board[x][y] == 0:
                valid = True
                break
            elif self.board[x][y] == player:
                break
            y -= 1
        return res if valid else []

    def get_diag_desc_left(self, x, y, player):
        x, y = x - 1, y - 1
        res = list()
        valid = False
        while x >= 0 and y >= 0:
            res.append(dict(x=x, y=y))
            if self.board[x][y] == 0:
                valid = True
                break
            elif self.board[x][y] == player:
                break
            x, y = x - 1, y - 1
        return res if valid else []

    def get_col_up(self, x, y, player):
        x -= 1
        res = list()
        valid = False
        while x >= 0:
            res.append(dict(x=x, y=y))
            if self.board[x][y] == 0:
                valid = True
                break
            elif self.board[x][y] == player:
                break
            x -= 1
        return res if valid else []

    def get_diag_asc_right(self, x, y, player):
        x, y = x - 1, y + 1
        res = list()
        valid = False
        while x >= 0 and y < 8:
            res.append(dict(x=x, y=y))
            if self.board[x][y] == 0:
                valid = True
                break
            elif self.board[x][y] == player:
                break
            x, y = x - 1, y + 1
        return res if valid else []

    def get_row_right(self, x, y, player):
        y += 1
        res = list()
        valid = False
        while y < 8:
            res.append(dict(x=x, y=y))
            if self.board[x][y] == 0:
                valid = True
                break
            elif self.board[x][y] == player:
                break
            y += 1
        return res if valid else []

    def get_diag_desc_right(self, x, y, player):
        x, y = x + 1, y + 1
        res = list()
        valid = False
        while x < 8 and y < 8:
            res.append(dict(x=x, y=y))
            if self.board[x][y] == 0:
                valid = True
                break
            elif self.board[x][y] == player:
                break
            x, y = x + 1, y + 1
        return res if valid else []

    def get_col_down(self, x, y, player):
        x += 1
        res = list()
        valid = False
        while x < 8:
            res.append(dict(x=x, y=y))
            if self.board[x][y] == 0:
                valid = True
                break
            elif self.board[x][y] == player:
                break
            x += 1
        return res if valid else []

    def get_diag_asc_left(self, x, y, player):
        x, y = x + 1, y - 1
        res = list()
        valid = False
        while x < 8 and y >= 0:
            res.append(dict(x=x, y=y))
            if self.board[x][y] == 0:
                valid = True
                break
            elif self.board[x][y] == player:
                break
            x, y = x + 1, y - 1
        return res if valid else []

    def play(self, move, player):
        get_tiles_func = [
            self.get_row_left_flip,
            self.get_diag_desc_left_flip,
            self.get_col_up_flip,
            self.get_diag_asc_right_flip,
            self.get_row_right_flip,
            self.get_diag_desc_right_flip,
            self.get_col_down_flip,
            self.get_diag_asc_left_flip
        ]
        neighbours = self.next_to_opponent(move['x'], move['y'], 1 if player == 2 else 2)
        tiles_to_flip = list()
        for neighbour in neighbours:
            tiles_to_flip.append(get_tiles_func[neighbour](move['x'], move['y'], player))
        for line in tiles_to_flip:
            for tile in line:
                self.board[tile['x']][tile['y']] = player
        self.board[move['x']][move['y']] = player

    def get_row_left_flip(self, x, y, player):
        y -= 1
        res = list()
        valid = False
        while y >= 0:
            res.append(dict(x=x, y=y))
            if self.board[x][y] == 0:
                break
            elif self.board[x][y] == player:
                valid = True
                break
            y -= 1
        return res if valid else []

    def get_diag_desc_left_flip(self, x, y, player):
        x, y = x - 1, y - 1
        res = list()
        valid = False
        while x >= 0 and y >= 0:
            res.append(dict(x=x, y=y))
            if self.board[x][y] == 0:
                break
            elif self.board[x][y] == player:
                valid = True
                break
            x, y = x - 1, y - 1
        return res if valid else []

    def get_col_up_flip(self, x, y, player):
        x -= 1
        res = list()
        valid = False
        while x >= 0:
            res.append(dict(x=x, y=y))
            if self.board[x][y] == 0:
                break
            elif self.board[x][y] == player:
                valid = True
                break
            x -= 1
        return res if valid else []

    def get_diag_asc_right_flip(self, x, y, player):
        x, y = x - 1, y + 1
        res = list()
        valid = False
        while x >= 0 and y < 8:
            res.append(dict(x=x, y=y))
            if self.board[x][y] == 0:
                break
            elif self.board[x][y] == player:
                valid = True
                break
            x, y = x - 1, y + 1
        return res if valid else []

    def get_row_right_flip(self, x, y, player):
        y += 1
        res = list()
        valid = False
        while y < 8:
            res.append(dict(x=x, y=y))
            if self.board[x][y] == 0:
                break
            elif self.board[x][y] == player:
                valid = True
                break
            y += 1
        return res if valid else []

    def get_diag_desc_right_flip(self, x, y, player):
        x, y = x + 1, y + 1
        res = list()
        valid = False
        while x < 8 and y < 8:
            res.append(dict(x=x, y=y))
            if self.board[x][y] == 0:
                break
            elif self.board[x][y] == player:
                valid = True
                break
            x, y = x + 1, y + 1
        return res if valid else []

    def get_col_down_flip(self, x, y, player):
        x += 1
        res = list()
        valid = False
        while x < 8:
            res.append(dict(x=x, y=y))
            if self.board[x][y] == 0:
                break
            elif self.board[x][y] == player:
                valid = True
                break
            x += 1
        return res if valid else []

    def get_diag_asc_left_flip(self, x, y, player):
        x, y = x + 1, y - 1
        res = list()
        valid = False
        while x < 8 and y >= 0:
            res.append(dict(x=x, y=y))
            if self.board[x][y] == 0:
                break
            elif self.board[x][y] == player:
                valid = True
                break
            x, y = x + 1, y - 1
        return res if valid else []

    def get_tiles_number(self):
        p1_tiles_number = len([elem for row in self.board for elem in row if elem == 1])
        p2_tiles_number = len([elem for row in self.board for elem in row if elem == 2])
        return p1_tiles_number, p2_tiles_number

if __name__ == '__main__':
    o = OthelloBoard()
