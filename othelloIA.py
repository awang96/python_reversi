from random import randint

BOARD_VALUES = [
    [500, -150, 30, 10, 10, 30, -150, 500],
    [-150, -250, 0, 0, 0, 0, -250, -150],
    [30, 0, 1, 2, 2, 1, 0, 30],
    [10, 0, 2, 16, 16, 2, 0, 10],
    [10, 0, 2, 16, 16, 2, 0, 10],
    [30, 0, 1, 2, 2, 1, 0, 30],
    [-150, -250, 0, 0, 0, 0, -250, -150],
    [500, -150, 30, 10, 10, 30, -150, 500]
]

class Player():

    def __init__(self, id):
        self.id = id

    def move(self, moves):
        moves = list(filter(lambda elem: elem, moves))
        if not moves:
            print('No moves available for player', self.id)
            return None
        move_results = self.get_moves_length(moves)
        max_result = max([move['move_result'] for move in move_results])
        moves = list(filter(lambda move: move['move_result'] == max_result, move_results))
        len_moves = len(moves)
        if len_moves == 1:
            return dict(x=moves[0]['x'], y=moves[0]['y'])
        else:
            index = randint(0, len_moves - 1)
            return dict(x=moves[index]['x'], y=moves[index]['y'])

    def get_moves_length(self, moves):
        moves_length = list()
        for move in moves:
            final_move = move[-1]
            index = self.same_move(moves_length, final_move)
            if index == -1:
                moves_length.append(dict(
                    x=final_move['x'],
                    y=final_move['y'],
                    move_result=self.eval_move(move)
                ))
            else:
                moves_length[index]['move_result'] += self.eval_move(move)
        return moves_length

    def same_move(self, moves, move):
        for counter, elem in enumerate(moves):
            if (move['x'], move['y']) == (elem['x'], elem['y']):
                return counter
        return -1

    def eval_move(self, move):
        return sum([BOARD_VALUES[elem['x']][elem['y']] for elem in move])
